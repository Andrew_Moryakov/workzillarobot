﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using BackZilla.Models;

namespace BackZilla
{
	public class WebApiApplication : System.Web.HttpApplication
	{
		protected void Application_Start()
		{
			string dest = AppDomain.CurrentDomain.SetupInformation.PrivateBinPath;
			string src = AppDomain.CurrentDomain.SetupInformation.ApplicationBase + "SqlServerCE\\x86";

			foreach (var file in Directory.GetFiles(src))
			{
				var fileinfo = new FileInfo(file);
				string destpath = Path.Combine(dest, fileinfo.Name);

				if (!File.Exists(destpath))
				{
					fileinfo.CopyTo(destpath);
				}
			}

			Database.SetInitializer(new AppDbInitializer());
			ApplicationDbContext db = new ApplicationDbContext();
			db.Database.Initialize(true);

			AreaRegistration.RegisterAllAreas();
			GlobalConfiguration.Configure(WebApiConfig.Register);
			FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
			RouteConfig.RegisterRoutes(RouteTable.Routes);
			BundleConfig.RegisterBundles(BundleTable.Bundles);
		}
	}
}
