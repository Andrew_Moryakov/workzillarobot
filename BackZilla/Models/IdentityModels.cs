﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;

namespace BackZilla.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }
    }

	public class Query
	{
		public Query()
		{
			
		}
		public Query(string keys)
		{
			Keys = keys;
			Date = DateTime.Now;
		}

		[Key]
		public int Id { get; set; }
		public string Keys { get; set; }
		public DateTime Date { get; set; }
	}

	public class UserActive
	{
		public UserActive()
		{

		}

		public UserActive(string uid, bool isActive, DateTime date, AppKey key)
		{
			Uid = uid;
			IsActive = isActive;
			Date = date;
			Key = key;
			KeyId = key.Id;
		}

		[Key]
		public string Uid { get; set; }

		public int KeyId { get; set; }
		[ForeignKey("KeyId")]
		public AppKey Key { get; set; }

	public bool IsActive { get; set; }
		public DateTime? Date { get; set; }
	}

	public class AppKey
	{
		public AppKey()
		{

		}

		public AppKey( string key)
		{
			Key = key;
		}

		[Key]
		public int Id { get; set; }

		public string Key { get; set; }

		public bool IsActive { get; set; } = true;
	}

	public class AllActive
	{
		public AllActive()
		{
			
		}
		[Key]
		public int Id { get; set; }
		public bool IsActive { get; set; } = true;
	}

	public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }
		public DbSet<Query> Queries { get; set; }
		public DbSet<UserActive> UsersActive { get; set; }
		public DbSet<AllActive> AllActivies { get; set; }
		public DbSet<AppKey> AppKeys { get; set; }
		public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }

	public class AppDbInitializer : CreateDatabaseIfNotExists<ApplicationDbContext>
	{
		protected override void Seed(ApplicationDbContext context)
		{
			context.AllActivies.Add(new AllActive());

			context.SaveChanges();

			base.Seed(context);
		}
	}
}