﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using BackZilla.Models;

namespace BackZilla.Controllers
{
	public class ValuesController : ApiController
	{
		static string GetMd5()
		{
			byte[] hash = Encoding.ASCII.GetBytes($"{DateTime.UtcNow.Year ^ 7}{DateTime.UtcNow.Month ^ 7}{DateTime.UtcNow.Day ^ 7}{DateTime.UtcNow.Hour ^ 7}{DateTime.UtcNow.Minute ^ 7}");
			MD5 md5 = new MD5CryptoServiceProvider();
			byte[] hashenc = md5.ComputeHash(hash);
			string result = "";
			foreach (var b in hashenc)
			{
				result += b.ToString("x2");
			}
			return result;
		}

		public async Task<string> GetActive()
		{
			return await Task.Factory.StartNew(() =>
					{
						using (ApplicationDbContext db = new ApplicationDbContext())
						{
							if (db.AllActivies.FirstOrDefault()?.IsActive == false)
								return null;


							return GetMd5();
						}
					}
				)
			;
		}
		// GET api/values
		[HttpGet]
		public async Task<string> Get(string id, string key)
		{
			id = id?.Replace($"{'"'}", "");
			key = key?.Replace($"{'"'}", "");

			return await Task.Factory.StartNew(() =>
					{
						using (ApplicationDbContext db = new ApplicationDbContext())
						{
							if (db.AllActivies.FirstOrDefault()?.IsActive == false)
								return null;

							var user = db.UsersActive.Include(el=>el.Key).FirstOrDefault(el => el.Uid == id);
							if (user == null )
							{
								AppKey appKeyFromDb = db.AppKeys.FirstOrDefault(el => el.Key == key);
								AppKey appKey = appKeyFromDb;
								if (appKeyFromDb == null)
								{
									appKey = new AppKey(key);
								}

								if (appKey.IsActive)
								{

									user = new UserActive(id, true, DateTime.Now, appKey);

									db.UsersActive.Add(user);
									//else
									//db.UsersActive.Add(new UserActive(id, true));
									db.SaveChanges();

									return GetMd5();
								}
							}

								try
								{
									return user?.IsActive == true && user.Key.IsActive? GetMd5() : null;
								}
								catch (Exception ex)
								{
									
								}
							

							return null;
						}
					}
				)
	        ;
        }
		
		[HttpGet]
		public async Task<string> GetDate(string id)
		{
			id = id?.Replace($"{'"'}", "");

			return await Task.Factory.StartNew(() =>
			{
				using (ApplicationDbContext db = new ApplicationDbContext())
				{
					var user = db.UsersActive.FirstOrDefault(el => el.Uid == id);
					if (user == null || DateTime.Now - user.Date > new TimeSpan(3, 0, 0, 0))
					{
						return "0";
					}

					return "1";
				}
			}
				)
			;
		}

		// POST api/values
		[HttpPost]
		public void Key([FromBody]object keys)
        {
	        using (ApplicationDbContext db = new ApplicationDbContext())
	        {
				db.Queries.Add(new Query((keys as dynamic).keys.ToString()));
		        db.SaveChanges();
	        }
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
