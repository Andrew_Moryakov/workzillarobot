﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CefSharp;
using CefSharp.Wpf;
using Newtonsoft.Json;

namespace WZRobo
{

	public class RegData
	{
		public string Key { get; set; }
		public string Email { get; set; }
		public static string _developerKey = "DreamPlace_MARKETERSDYNASTY_developerKey_2017_8aShfolR6w8";
		[JsonIgnore]
		public string Machine
		{
			get { return EasyEncryption.MD5.ComputeMD5Hash(GetCpuInfo() + GetHddId() + Environment.MachineName); }
		}

		public void SaveToFile()
		{
			Properties.Settings.Default.data = JsonConvert.SerializeObject(this);
			Properties.Settings.Default.Save();
			//File.WriteAllText("data", JsonConvert.SerializeObject(this));
		}

		public void OpenFile()
		{
			string key = null;

			try
			{
				key = Properties.Settings.Default.data;

				RegData regData = JsonConvert.DeserializeObject<RegData>(key);

				this.Key = regData.Key;
				this.Email = regData.Email;
			}
			catch (Exception ex)
			{
				return;
			}
		}

		private string GetHddId()
		{
			string drive = "C";
			ManagementObject dsk = new ManagementObject(
				@"win32_logicaldisk.deviceid=""" + drive + @":""");
			dsk.Get();
			return dsk["VolumeSerialNumber"].ToString();
		}

		private string GetCpuInfo()
		{
			ManagementClass mc = new ManagementClass("win32_processor");
			ManagementObjectCollection moc = mc.GetInstances();

			foreach (ManagementObject mo in moc)
			{
				return mo.Properties["processorID"].Value.ToString();
			}

			throw new Exception("Не удалось получить информацию о процессоре");
		}


		public static bool RegDataIsTrue(RegData regData)
		{
			//string email = regData.Email;

			//string md5HashEmil = EasyEncryption.MD5.ComputeMD5Hash(email);
			//string md5HashMashineName = EasyEncryption.MD5.ComputeMD5Hash(regData.Machine);

			//   string key = "";
			//   for (int i = 0; i < md5HashEmil.Length; i++)
			//   {
			//    key += md5HashEmil[i] ^ 3 & md5HashMashineName[i] ^ 7;
			//   }
			List<string> keys = new List<string>();
			for (int i = 0; i < 1000; i++)
			{
				keys.Add(string.Join("", EasyEncryption.MD5.ComputeMD5Hash("workZillaKeyGenerator" + i + "workzillaBot").Select(el => el ^ 3).Distinct()));
			}

			return keys.Any(el => el == regData.Key) || regData.Key == RegData._developerKey;
		}
	}

	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private string[] matches;
		private static Timer timer = new Timer();
		private string[] hashSet = new string[0];
		private bool isToSend = false;
		private string _message = "";

		public MainWindow()
		{
			InitializeComponent();

			//CefSettings settings = new CefSettings();
			//Cef.Initialize(settings);
			TextBoxUrl.Text = "http://client.work-zilla.com/executive.aspx";
			TextBoxKeyWords.Text = @"Ключевые, слова";
			Chrome.Load(TextBoxUrl.Text);
			

			RegData regData = new RegData();
			regData.OpenFile();
//			button1.Visible = regData.Key == RegData._developerKey;
//#if DEBUG
//			button1.Visible = true;
//#endif
		}

		private void Loop(object sender, EventArgs e)
		{
			//     if (Chrome.Address != "http://client.work-zilla.com/executive.aspx")
			//     {
			//      btnStart.Enabled = true;
			//      btnStop.Enabled = false;
			//timer.Stop();
			//      return;
			//     }

			try
			{
				var keys = TextBoxKeyWords.Text.Split(',').Select(s => s.Trim()).ToArray();
				var search = string.Join(",", keys.Select(s => $"\"{s}\"").ToArray());

				var findedOrder = hashSet == null ? "" : string.Join(",", hashSet?.Select(s => $"\"{s}\"")?.ToArray());
				string sendScript = isToSend == true ? @"var acceptButton = container.find('.cf-ok');
															acceptButton.trigger(""click"");" : "";



				var task = Chrome.EvaluateScriptAsync(
					@"
var countClick =0;
var keyWords = [" + search + @"]
var ids = [" + findedOrder + @"];
try{
for (var i = 0; i < keyWords.length; i++)
                        {
console.log(keyWords[i]);
		$('span:contains(' + keyWords[i]+ ')').each(function(index) {

				var container = $(this).parents('.workorder');

				if (container !== undefined || container !== null) {
					if (!ids.includes(container.attr('id'))) {
		if(container.find('.cf-ok').length === 0) {
						var acceptCheckBox = container.find('.wo-accept');
						acceptCheckBox.trigger(""click"");

						setTimeout(function() {
								var acceptButton = container.find('.cf-ok');
								acceptButton.trigger(""click"");
						console.log(""countClick"" "": "" + ""Clicked "" + container.attr('id'));
							},
							700);
							$(this).css(""background-color"", ""blue"");
						$(this).css(""color"", ""white"");
					var textArea = container.find('.text-input-area');
					textArea.val('" + _message.Replace("\r\n", "\\r\\n").Replace("\"", "\\'") + @"');
					console.log(""textArea: "" + textArea);
					ids.push(container.attr('id'));
					}
					}

					if (ids.includes(container.attr('id'))) {
						var acceptButton = container.find('.cf-ok');
						if (acceptButton !== undefined){

							setTimeout(function() {
								" + sendScript + @"
								console.log(""countClick"" "": "" + ""Sended "" + container.attr('id'));
							},
							700);
							$(this).css(""background-color"", ""green"");
							//$(this).css(""color"", ""blak"");
							console.log(""sending"");
						}
						}
				}
			});
	}
var reslt = function(value) {
	return value;
}
} catch(err){
 console.log('Ошибка ' + e.name + ':' + e.message + '\n' + e.stack);
}

reslt(ids);
		
			");

				task.ContinueWith(t =>
				{
					hashSet = ((List<object>)t.Result?.Result)?.Select(el => (string)el)?.ToArray();
					if (t?.Result?.Success == false)
					{
				//Chrome.Reload();
			}
				});

				LabelFinded.Content = $"Finded {hashSet?.Length}";
			}
			catch (Exception ex)
			{
				File.WriteAllText(JsonConvert.SerializeObject(ex), DateTime.Now.ToString() + ".log");
			}
		}

		private void ButtonStart_Click(object sender, RoutedEventArgs e)
		{

			ButtonStart.IsEnabled = false;
			ButtonStop.IsEnabled = true;
			timer.Interval = 10000;
			//timer.Tick += Loop;
			//timer.Start();
		}

		private void ButtonStop_Click(object sender, RoutedEventArgs e)
		{
			ButtonStart.IsEnabled = true;
			ButtonStop.IsEnabled = false;
			timer.Stop();
		}

		private void ButtonGo_Click(object sender, RoutedEventArgs e)
		{
			Chrome.Load(TextBoxUrl.Text);
		}

		private void ButtonLeft_Click(object sender, RoutedEventArgs e)
		{
			if (Chrome.CanGoForward)
				Chrome.Forward();
		}

		private void ButtonRight_Click(object sender, RoutedEventArgs e)
		{
			if (Chrome.CanGoBack)
				Chrome.Back();
		}

		private void MenuItem_Click(object sender, RoutedEventArgs e)
		{

		}
	}
}
