﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WorkZillaBotKeys
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
		}

		private void button_Click(object sender, RoutedEventArgs e)
		{
			string email = TextBoxEmail.Text; ;

			string md5HashEmil = EasyEncryption.MD5.ComputeMD5Hash(email);
			string md5HashMashineName = EasyEncryption.MD5.ComputeMD5Hash(TextBoxMashineName.Text);

			string key = "";
			for (int i = 0; i < md5HashEmil.Length; i++)
			{
				key += md5HashEmil[i] ^ 7 & md5HashMashineName[i] ^ 2;
			}

			TextBoxKey.Text = key;
		}
	}
}
