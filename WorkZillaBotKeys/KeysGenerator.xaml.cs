﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WorkZillaBotKeys
{
	/// <summary>
	/// Interaction logic for KeysGenerator.xaml
	/// </summary>
	public partial class KeysGenerator : Window
	{
		public KeysGenerator()
		{
			InitializeComponent();
		}
		private void button_Click(object sender, RoutedEventArgs e)
		{
		List<string> keys = new List<string>();

			for (int i = 0; i < 1000; i++)
			{
				keys.Add(string.Join("", EasyEncryption.MD5.ComputeMD5Hash("workZillaKeyGenerator" + i + "workzillaBot").Select(el=>el ^3 ).Distinct()));
			}

			textBlock_Copy.Text = keys.Count.ToString();
			textBox.Text = string.Join(Environment.NewLine, keys);
		}
	}
}
