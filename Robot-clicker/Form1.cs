﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using CefSharp;
using CefSharp.WinForms;
using Newtonsoft.Json;
using RestSharp;
using Timer = System.Windows.Forms.Timer;

namespace Robot_clicker
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
			_keyWords = "adwords, google adwords, google.adwords, g.adwords, ga, гугл эдвордс, гугл адвордс, адвордс, эдвордс";
		}

        private ChromiumWebBrowser chrome;
        private string[] matches;
        private static Timer timer = new Timer();
	    private string[] hashSet = new string[0];
	    private bool isToSend = false;

	    private string _keyWords = "";
        private void Form1_Load(object sender, EventArgs e)
        {
            CefSettings settings = new CefSettings();
            Cef.Initialize(settings);
            txtUrl.Text = "http://client.work-zilla.com/executive.aspx";
            keyWords.Text = @"Ключевые, слова";
            chrome = new ChromiumWebBrowser(txtUrl.Text);
            this.pContainer.Controls.Add(chrome);
            chrome.Dock = DockStyle.Fill;
            chrome.AddressChanged += Chrome_AddressChanged;

			RegData regData = new RegData();
			regData.OpenFile();
	        button1.Visible = regData.Key == RegData._developerKey;
#if DEBUG
			button1.Visible = true;
#endif
		}

        private void Chrome_AddressChanged(object sender, AddressChangedEventArgs e)
        {
            Invoke(new MethodInvoker(() =>
            {
                txtUrl.Text = e.Address;
            }));
        }

        private void btnGo_Click(object sender, EventArgs e)
        {
            chrome.Load(txtUrl.Text);
        }

	    private string _key = null;
        private void btnStart_Click(object sender, EventArgs e)
        {
	        PostHttpStatistic();
			btnStart.Enabled = false;
	        btnStop.Enabled = true;

			timer.Interval = 10000;
			timer.Tick += Loop;
			timer.Start();

			//var js = PrepareJS();
			//var task = chrome.EvaluateScriptAsync(js);
			//task.ContinueWith(t =>
			//{
			//    var responce = JsonConvert.SerializeObject(t.Result.Result);
			//    var result = JsonConvert.DeserializeObject<Dictionary<string, object>>(responce);
			//    hashSet = result.Keys.ToArray();
			//});
		}

	    private void PostHttpStatistic()
		{
			try
			{
				var client = new RestClient("http://localhost:64886/api/Values/Key");
				var request = new RestRequest(Method.POST);
				request.AddHeader("cache-control", "no-cache");
				request.AddHeader("content-type", "application/json");
				request.AddParameter("application/json", $"{{\"keys\":\"{_keyWords}\"}}", ParameterType.RequestBody); //	request.AddParameter("application/json", $"{{\"keys\":\"{keyWords.Text}\"}}", ParameterType.RequestBody);
				IRestResponse response = client.Execute(request);
			}
			catch (Exception ex)
			{
				
			}
		}

	    //private string[] GetMinusWords(string words)
	    //{
		    
	    //}

        private void Loop(object sender, EventArgs e)
        {

	        try
	{
		var keys = _keyWords.Split(',').Select(s => s.Trim()).ToArray();
		var search = string.Join(",", keys.Select(s => $"\"{s}\"").ToArray());

		var findedOrder = hashSet == null ? "" : string.Join(",", hashSet?.Select(s => $"\"{s}\"")?.ToArray());
		string sendScript = isToSend == true ? @"var acceptButton = container.find('.js-confirmAcception');
															acceptButton.trigger(""click"");" : "";

			

		var task = chrome.EvaluateScriptAsync(
			@"
var countClick =0;
var keyWords = [" + search + @"]
var ids = [" + findedOrder + @"];
try{

	//$('span.wo-subject').filter(function(){
	//		$(this).addClass('noselected');
	//		});

console.log('################### Start script #########################');
for (var i = 0; i < keyWords.length; i++)
                        {
		$('span.js-jobNameText').filter(function(){
			if($(this).text().toUpperCase().indexOf( keyWords[i].toUpperCase()) !== -1){
				//$(this).removeClass('noselected');
				console.log('Finded element with text: ' + $(this).text());
				console.log('Finded element with key: ' + keyWords[i]);
				var container = $(this).parents('.executive_search_one');
				console.log('Main container ' + container);
				console.log('Order №' + container.attr('orderid'));

				if (container !== undefined || container !== null) {
					if (!ids.includes(container.attr('orderid'))) {
						console.log('Order is open ' + container.find('.js-acceptOrderBtn').length!=0);
						if(container.find('.js-acceptOrderBtn').length === 0) {
							console.log('****************** Accept bloc ******************');
							var acceptCheckBox = container.find('.job_descrition');
							console.log(""Accept container finded "");
							acceptCheckBox.trigger(""click"");

							setTimeout(function() {
									var acceptButton = container.find('.js-acceptOrderBtn');
									acceptButton.trigger(""click"");
								}, 700);

							$(this).css(""background-color"", ""blue"");
							$(this).css(""color"", ""white"");
							console.log('Bloc kwith text: ' + $(this).text() + 'selectd.');
							//var textArea = container.find('textarea');//$('#txtChatArea'))//'#txtChatArea');
							console.log('Text area found ' + container.find('textarea') + 'with value ' + container.find('textarea').val());
								setTimeout(function() {
									$('textarea').trigger('click');
									$('textarea').text('" + textBox1.Text.Replace("\r\n", "\\r\\n").Replace("\"", "\\'") + @"');
								}, 700);
							$('textarea').text('asdsd');//" + textBox1.Text.Replace("\r\n", "\\r\\n").Replace("\"", "\\'") + @"');
							}
						}

					if (!ids.includes(container.attr('orderid')) && '" + isToSend + @"' =='True') {
						console.log('****************** send block ******************');
						var acceptButton = container.find('.job_descrition');
						if (acceptButton !== undefined){
							console.log(""Send container finded "" + container.attr('id'));
							setTimeout(function() {
								" + sendScript + @"
								console.log(""countClick: Sended "" + container.attr('id'));
							},
							300);
							ids.push(container.attr('id'));
							$(this).css(""background-color"", ""green"");
							console.log(""sending"");
						}
						}
					}
				}
			});
	}

//console.log('****************** bloc for hide non selected ******************');
//			$('span.noselected').filter(function(){
//						$(this).closest( 'div.workorder' ).css(""display"", ""none"");
//			});

var reslt = function(value) {
	return value;
}
} catch(err){
 console.log('Ошибка ' + err.name + ':' + err.message + '\n' + err.stack);
}

reslt(ids);
		
			");
				
		task.ContinueWith(t =>
		{
			hashSet = ((List<object>) t.Result?.Result)?.Select(el => (string) el)?.ToArray();
			if (t?.Result?.Success == false)
			{
				//chrome.Reload();
			}
		});

		label3.Text = $"Finded {hashSet?.Length}";
	}
	catch (Exception ex)
	{
		File.WriteAllText(JsonConvert.SerializeObject(ex), DateTime.Now.ToString() + ".log");
	}
}

        private void btnStop_Click(object sender, EventArgs e)
        {
	        btnStart.Enabled = true;
			btnStop.Enabled = false;
			timer.Stop();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            chrome.Refresh();
        }

        private void btnForward_Click(object sender, EventArgs e)
        {
            if(chrome.CanGoForward)
                chrome.Forward();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            if(chrome.CanGoBack)
                chrome.Back();
        }

        private void btnDev_Click(object sender, EventArgs e)
        {
            chrome.ShowDevTools();
        }
        
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Cef.Shutdown();
        }

        private string PrepareJS()
        {
            var keys = keyWords.Text.Split(',').Select(s=> s.Trim()).ToArray();
            var search = string.Join(",", keys.Select(s => $"\"{s}\"").ToArray());

			string sendScript = isToSend == true
		        ? @"order.getElementsByClassName(""cf-ok"")[0].dispatchEvent(new MouseEvent(""click"", {
						                             ""view"": window,
						                             ""bubbles"": true,
						                             ""cancelable"": false
						                         }));"
		        : "";


			var sb = new StringBuilder();
            sb.Append(@"
                var HashSet = function() {
                var set = { };
                this.add = function(key)
                    {
                        set[key] = true;
                    };
                this.remove = function(key)
                    {
                        delete set[key];
                    };
                this.contains = function(key)
                    {
                        return set.hasOwnProperty(key);
                    };
                this.showAll = function(){
                      return set;
                    };
this.thisArray = set;
                };
                var set = new HashSet();
                var search = [");
            sb.Append(search);
            sb.Append(@"
                ];
                function findOrders(string)
                    {
                        var matches = document.evaluate(""//span[contains(., '"" + string + ""')]"", document.documentElement, null,
                      XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
                        var orders = [];
                        for (var i = 0; i < matches.snapshotLength; i++)
                        {
                            var el = matches.snapshotItem(i);
                            while ((el = el.parentElement) && !el.classList.contains(""workorder"")) ;
                            orders.push(el);
                        }
                        return orders;
                    }
            ");
			if(hashSet!=null)
            foreach (var s in hashSet)
            {
                sb.AppendLine($"set.add(\"{s}\");");
            }

			sb.Append(@"
                function main(){
//try{
                    console.log(""NEW ITERATION"");
                    for (var j = 0; j<search.length; j++) {
console.log(""search: "" + search[j]);
console.log(""search length: "" + search.length);
                        var orders = findOrders(search[j]);
                        for (var i = 0; i<orders.length; i++) {
                            var order = orders[i];
if (order === null || order=== 'undefined'){
console.log(""BUG undefined order: "" + search[j]);
}
                            if (order == null || order === 'undefined' || set.contains(order.id)) {
console.log(""break with: "" + search[j]);
                                continue;
                            }
                            set.add(order.id);
console.log(""Added order: "" + search[j]);
console.log(""order length: "" + set.thisArray.length);
try{                           
order.getElementsByClassName(""wo-accept"")[0].dispatchEvent(new MouseEvent(""click"", {
                                ""view"": window,
                                        ""bubbles"": true,
                                        ""cancelable"": false
                                    }));
}catch(error){
console.log(""errror wo-accept"");
console.log(errror);
console.log(order.getElementsByClassName(""wo-accept""))
}

try{
                            order.getElementsByClassName(""cf-ok"")[0].dispatchEvent(new MouseEvent(""click"", {
                                ""view"": window,
                                        ""bubbles"": true,
                                        ""cancelable"": false
                                    }));
}catch(error){
console.log(""errror cf-ok"");
console.log(errror);
console.log(order.getElementsByClassName(""cf-ok""))
}

                            order.getElementsByTagName(""textarea"")[0].value = '" + textBox1.Text + @"';" +

					sendScript +
						//if (isToSend)
						//{
						//	sb.Append(@"set.add(order.id);");
						//}
						@"}
                    }
                    return set.showAll();
console.log(set.showAll());
//}catch(err){
//console.log(""Finaly error"");
//console.log(err);
//}
                }
                main();"
			);

			//@"order.getElementsByClassName(""cf-ok"")[0].dispatchEvent(new MouseEvent(""click"", {
   //                             ""view"": window,
   //                             ""bubbles"": true,
   //                             ""cancelable"": false
   //                         }));
            return sb.ToString();
        }

	    private bool isDown = false;
		private void button3_Click(object sender, EventArgs e)
		{
		
		}

		private void checkBox1_CheckedChanged(object sender, EventArgs e)
		{
			//if (checkBox1.Checked == true)
			//{
			//	checkBox1.Checked = false;
			//}
			//else
			//{
			//	checkBox1.Checked = true;
			//}
		}


		private void button2_Click(object sender, EventArgs e)
		{
			
		}

		private void messageToolStripMenuItem_Click(object sender, EventArgs e)
		{
		
		}

		private void textToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (!isDown)
			{
				//button3.Text = "Message ↓";
				isDown = true;
				pContainer.Location = new Point(pContainer.Location.X, pContainer.Location.Y + 400);
			}
			else
			{
				//button3.Text = "Message ↑";
				isDown = false;
				pContainer.Location = new Point(pContainer.Location.X, pContainer.Location.Y - 400);
			}
		}

		private void toSendToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (isToSend)
			{
				isToSend = false;
			}
			else
			{
				isToSend = true;
			}
		}

		SaveFileDialog sfd = new SaveFileDialog();
		private void saveToolStripMenuItem1_Click(object sender, EventArgs e)
		{
			sfd.Filter = "WorkZillaRobotSetting|*.json";
			if (sfd.ShowDialog() == DialogResult.OK)
			{
				string jsonSetting = JsonConvert.SerializeObject(new BotSetting(_keyWords, textBox1.Text));

				string filename = sfd.FileName;
				System.IO.File.WriteAllText(filename, jsonSetting);
			}
		}

		OpenFileDialog ofd = new OpenFileDialog();
		private void openToolStripMenuItem1_Click(object sender, EventArgs e)
		{
			ofd.Filter = "WorkZillaRobotSetting|*.json";
			if (ofd.ShowDialog() == DialogResult.OK)
			{
				string filename = ofd.FileName;
				string stringSetting  = System.IO.File.ReadAllText(filename);

				BotSetting bs = JsonConvert.DeserializeObject<BotSetting>(stringSetting);

				textBox1.Text = bs.Message;
				_keyWords = bs.KeyWords;
			}
		}



		private void enterTheLicenseKeyToolStripMenuItem_Click(object sender, EventArgs e)
	{
			new AboutBox1().Show();
		}

		private void Form1_Paint(object sender, PaintEventArgs e)
		{
			if (_isRepaint)
			{
				chrome.Location = new Point(0, flowLayoutPanel1.Size.Height + menuStrip1.Size.Height);
				chrome.Size = new Size(this.Height - flowLayoutPanel1.Size.Height + menuStrip1.Size.Height, this.Width);
			}
		}

	    private bool _isRepaint = false;
		private void repaintToolStripMenuItem_Click(object sender, EventArgs e)
		{
			_isRepaint = !_isRepaint;
		}

		private void instagramToolStripMenuItem_Click(object sender, EventArgs e)
		{
			_keyWords = "inst, инста, Инста, Inst";
		}

		private void vkToolStripMenuItem_Click(object sender, EventArgs e)
		{
			_keyWords = "директ, яндекс директ, direct, yandex direct, яндекс.директ, я.директ, яд, я директ";
		}

		private void googleAdwordsToolStripMenuItem_Click(object sender, EventArgs e)
		{
			_keyWords = "adwords, google adwords, google.adwords, g.adwords, ga, гугл эдвордс, гугл адвордс, адвордс, эдвордс";
		}
	}
	public class BotSetting
	{
		public BotSetting(string keys, string message)
		{
			KeyWords = keys;
			Message = message;
		}

		public string KeyWords { get; set; }
		public string Message { get; set; }
	}
}
