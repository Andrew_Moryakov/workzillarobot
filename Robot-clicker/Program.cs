﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;
using Newtonsoft.Json;
using NLog;
using RestSharp;

namespace Robot_clicker
{
    static class Program
    {
	    public static Form MainForm; 
	    private static bool _isActive = false;
		private static bool _isServerActive = false;
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
	    static void Main()
	    {
			Logger logger = LogManager.GetCurrentClassLogger();

			try
		    {
			    Application.EnableVisualStyles();
			    Application.SetCompatibleTextRenderingDefault(false);
			    AboutBox1 aboutBox = new AboutBox1();
			    MainForm = new Form1();

				RegData regData = new RegData();
			    if (regData.Email != null && regData.Key != null && RegData.RegDataIsTrue(regData))
			    {
					logger.Info( "Key active.");
					_isActive = RegData.CheckActiveUseServer(regData.Email, regData.Key);
					logger.Info(_isActive ? "Sok." : "Sof");
				}
			    else
			    {
					var startFate = Read("wzrobodatestart");
					if (startFate != null)
					{
						_isActive = CheckTrialByLocalTime(startFate);
						logger.Info(_isActive ? "Trial active." : "Trial ended.");
					}
					else
					{
						string date = GetNetworkTime().ToString(CultureInfo.InvariantCulture);
						Write("wzrobodatestart", date);
						logger.Info("First Startup. Trial start.");
						_isActive = true;
					}
				}

			    var isChrashProgram = RegData.CheckGlobalActive();
				logger.Info("_isActive " + _isActive);
				MainForm.Enabled = _isActive;
			    if (!_isActive)
				    aboutBox.Show();

				if (isChrashProgram)
				Application.Run(MainForm);


				
		    }
		    catch (Exception ex)
		    {
				logger.Error(ex, "Error is occurred.");
			    MessageBox.Show("Произошла ошибка");
		    }
	    }

		

		private static bool CheckTrialByLocalTime(string startDate)
	    {
		    DateTime dt;
		    if (DateTime.TryParse(startDate, out dt))
		    {
			    if (GetNetworkTime() - dt > new TimeSpan(3, 0, 0, 0))
			    {
				    return false;
			    }
		    }

		    return true;
	    }


	    public static
		    DateTime GetNetworkTime()
		{
			//default Windows time server
			const string ntpServer = "time.windows.com";

			// NTP message size - 16 bytes of the digest (RFC 2030)
			var ntpData = new byte[48];

			//Setting the Leap Indicator, Version Number and Mode values
			ntpData[0] = 0x1B; //LI = 0 (no warning), VN = 3 (IPv4 only), Mode = 3 (Client Mode)

			var addresses = Dns.GetHostEntry(ntpServer).AddressList;

			//The UDP port number assigned to NTP is 123
			var ipEndPoint = new IPEndPoint(addresses[0], 123);
			//NTP uses UDP
			var socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

			socket.Connect(ipEndPoint);

			//Stops code hang if NTP is blocked
			socket.ReceiveTimeout = 3000;

			socket.Send(ntpData);
			socket.Receive(ntpData);
			socket.Close();

			//Offset to get to the "Transmit Timestamp" field (time at which the reply 
			//departed the server for the client, in 64-bit timestamp format."
			const byte serverReplyTime = 40;

			//Get the seconds part
			ulong intPart = BitConverter.ToUInt32(ntpData, serverReplyTime);

			//Get the seconds fraction
			ulong fractPart = BitConverter.ToUInt32(ntpData, serverReplyTime + 4);

			//Convert From big-endian to little-endian
			intPart = SwapEndianness(intPart);
			fractPart = SwapEndianness(fractPart);

			var milliseconds = (intPart * 1000) + ((fractPart * 1000) / 0x100000000L);

			//**UTC** time
			var networkDateTime = (new DateTime(1900, 1, 1, 0, 0, 0, DateTimeKind.Utc)).AddMilliseconds((long)milliseconds);

			return networkDateTime.ToLocalTime();
		}

		// stackoverflow.com/a/3294698/162671
		static uint SwapEndianness(ulong x)
		{
			return (uint)(((x & 0x000000ff) << 24) +
						   ((x & 0x0000ff00) << 8) +
						   ((x & 0x00ff0000) >> 8) +
						   ((x & 0xff000000) >> 24));
		}

		public static bool Write(string KeyName, object Value)
		{
				// Setting
				RegistryKey rk = Registry.CurrentUser;
				// I have to use CreateSubKey 
				// (create or open it if already exits), 
				// 'cause OpenSubKey open a subKey as read-only
				RegistryKey sk1 = rk.CreateSubKey("Software\\DreamPlace\\WZR" + Application.ProductName);
				// Save the value
				sk1.SetValue(KeyName.ToUpper(), Value);

				return true;
		}

		public static string Read(string KeyName)
		{
			// Opening the registry key
			RegistryKey rk = Registry.CurrentUser;
			// Open a subKey as read-only
			RegistryKey sk1 = rk.OpenSubKey("Software\\DreamPlace\\WZR" + Application.ProductName);
			// If the RegistrySubKey doesn't exist -> (null)
			if (sk1 == null)
			{
				return null;
			}
			else
			{
					return (string)sk1.GetValue(KeyName.ToUpper());
			}
		}
	}
}
