﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Management;
using System.Reflection;
using System.Security;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using NLog;
using RestSharp;

namespace Robot_clicker
{
	partial class AboutBox1 : Form
	{
		private RegData reg;
		public AboutBox1()
		{
			InitializeComponent();
			this.Text = String.Format("About: {0}", AssemblyTitle);
			this.labelProductName.Text = AssemblyProduct;
			this.labelVersion.Text ="Version: " + String.Format("Version: {0}", AssemblyVersion);
			this.labelCopyright.Text = AssemblyCopyright;
			this.labelCompanyName.Text = String.Format("Company developer: {0}", AssemblyCompany);
			logoPictureBox.Image = Properties.Resources._10;


			reg = new RegData();
			textBoxMachineName.Text = reg.Machine;
			reg.OpenFile();

			if (reg.Email != null)
			{
				textBoxEmail.Text = reg.Email;
				textBoxKey.Text = reg.Key;
			}

			if (RegData._developerKey == reg.Key)
				buttonClean.Visible = true;
		}

		#region Assembly Attribute Accessors

		public string AssemblyTitle
		{
			get
			{
				object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
				if (attributes.Length > 0)
				{
					AssemblyTitleAttribute titleAttribute = (AssemblyTitleAttribute)attributes[0];
					if (titleAttribute.Title != "")
					{
						return titleAttribute.Title;
					}
				}
				return System.IO.Path.GetFileNameWithoutExtension(Assembly.GetExecutingAssembly().CodeBase);
			}
		}

		public string AssemblyVersion
		{
			get
			{
				return Assembly.GetExecutingAssembly().GetName().Version.ToString();
			}
		}

		public string AssemblyDescription
		{
			get
			{
				object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false);
				if (attributes.Length == 0)
				{
					return "";
				}
				return ((AssemblyDescriptionAttribute)attributes[0]).Description;
			}
		}

		public string AssemblyProduct
		{
			get
			{
				object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
				if (attributes.Length == 0)
				{
					return "";
				}
				return ((AssemblyProductAttribute)attributes[0]).Product;
			}
		}

		public string AssemblyCopyright
		{
			get
			{
				object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
				if (attributes.Length == 0)
				{
					return "";
				}
				return ((AssemblyCopyrightAttribute)attributes[0]).Copyright;
			}
		}

		public string AssemblyCompany
		{
			get
			{
				object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);
				if (attributes.Length == 0)
				{
					return "";
				}
				return ((AssemblyCompanyAttribute)attributes[0]).Company;
			}
		}
		#endregion
	
		private void button1_Click(object sender, EventArgs e)
		{
			var regDta = new RegData
			{
				Email = RegData.IsValidEmail(textBoxEmail.Text)?textBoxEmail.Text:null,
				Key = textBoxKey.Text
			};

			bool isActive = false;

			if(!string.IsNullOrEmpty(regDta.Email) && !string.IsNullOrEmpty(regDta.Key) && regDta.Email.Length > 5 && regDta.Key.Length > 10)
			isActive = RegData.CheckActiveUseServer(regDta.Email, regDta.Key) && RegData.CheckTrial(regDta.Email);

			if (!isActive)
				return;

			regDta.SaveToFile();

			bool isCorrect = false;
			RegData regData = new RegData();
			regData.OpenFile();
			if (regData.Email != null && regData.Key != null && RegData.RegDataIsTrue(regData))
			{
				isCorrect = true;
			}

			if (isCorrect)
			{
				Program.MainForm.Enabled = true;
				this.Close();
			}
		}


		private void okButton_Click(object sender, EventArgs e)
		{
			var regDta = new RegData
			{
				Email = RegData.IsValidEmail(textBoxEmail.Text) ? textBoxEmail.Text : null,
				Key = textBoxKey.Text
			};

			regDta.SaveToFile();
			this.Close();
		}

		private void buttonClean_Click(object sender, EventArgs e)
		{
			Properties.Settings.Default.data = null;
			Properties.Settings.Default.Save();
		}

		private void AboutBox1_Load(object sender, EventArgs e)
		{

		}
	}

	public class RegData
	{
		public string Key { get; set; }
		public string Email { get; set; }
		public static string _developerKey = "DreamPlace_MARKETERSDYNASTY_developerKey_2017_8aShfolR6w8";

		public RegData()
		{
			Email = null;
		}

		[JsonIgnore]
		public string Machine
		{
			get { return EasyEncryption.MD5.ComputeMD5Hash(GetCpuInfo() + GetHddId() + Environment.MachineName); }
		}
		public static bool IsValidEmail(string email)
		{
			try
			{
				var addr = new System.Net.Mail.MailAddress(email);
				return addr.Address == email;
			}
			catch
			{
				return false;
			}
		}

		private static string _uri = "backzilla20170323015527.azurewebsites.net/api/Values";
		public static bool CheckActiveUseServer(string email, string key)
		{
			var client = new RestClient($"http://{_uri}/Get/?id=\"{email}\"&key=\"{key}\"");
			var request = new RestRequest(Method.GET);
			//request.AddHeader("cache-control", "no-cache");
			IRestResponse response = client.Execute(request);
			LogManager.GetCurrentClassLogger().Info(response?.ErrorException, response?.Content);

			return response?.Content?.Replace($"{'"'}", "") == GetMd5();
		}
		
		public static bool CheckGlobalActive()
		{
			var client = new RestClient($"http://{_uri}/GetActive");
			var request = new RestRequest(Method.GET);
			//request.AddHeader("cache-control", "no-cache");
			IRestResponse response = client.Execute(request);
			LogManager.GetCurrentClassLogger().Info(response?.ErrorException, response?.Content);

			return response?.Content?.Replace($"{'"'}", "") == GetMd5();
		}

		static string GetMd5()
		{
			byte[] hash = Encoding.ASCII.GetBytes($"{DateTime.UtcNow.Year ^ 7}{DateTime.UtcNow.Month ^ 7}{DateTime.UtcNow.Day ^ 7}{DateTime.UtcNow.Hour ^ 7}{DateTime.UtcNow.Minute ^ 7}");
			MD5 md5 = new MD5CryptoServiceProvider();
			byte[] hashenc = md5.ComputeHash(hash);
			string result = "";
			foreach (var b in hashenc)
			{
				result += b.ToString("x2");
			}
			return result;
		}

		public static bool CheckActiveUseServer()
		{
			var client = new RestClient($"http://{_uri}/GetActive/");
			var request = new RestRequest(Method.GET);
			//request.AddHeader("cache-control", "no-cache");
			IRestResponse response = client.Execute(request);
			LogManager.GetCurrentClassLogger().Info(response?.ErrorException, response?.Content);

			return response?.Content?.Replace($"{'"'}", "") == "1";
		}

		public static bool CheckTrial(string email)
		{
			var client = new RestClient($"http://{_uri}/GetDate/?id=\"{email}\"");
			var request = new RestRequest(Method.GET);
			request.AddHeader("cache-control", "no-cache");
			IRestResponse response = client.Execute(request);
			LogManager.GetCurrentClassLogger().Info(response?.ErrorException, response?.Content);

			return response?.Content?.Replace($"{'"'}", "") == "1";
		}

		public void SaveToFile()
		{
			Properties.Settings.Default.data = JsonConvert.SerializeObject(this);
			Properties.Settings.Default.Save();
			//File.WriteAllText("data", JsonConvert.SerializeObject(this));
		}

		public void OpenFile()
		{
			string key = null;
			
			try
			{
				key = Properties.Settings.Default.data;

				RegData regData = JsonConvert.DeserializeObject<RegData>(key);

				this.Key = regData.Key;
				this.Email = IsValidEmail(regData.Email) ? regData.Email : null;
			}
			catch (Exception ex)
			{
				return;
			}
		}

		private string GetHddId()
		{
			string drive = "C";
			ManagementObject dsk = new ManagementObject(
				@"win32_logicaldisk.deviceid=""" + drive + @":""");
			dsk.Get();
			return dsk["VolumeSerialNumber"].ToString();
		}

		private string GetCpuInfo()
		{
			ManagementClass mc = new ManagementClass("win32_processor");
			ManagementObjectCollection moc = mc.GetInstances();

			foreach (ManagementObject mo in moc)
			{
				return mo.Properties["processorID"].Value.ToString();
			}

			throw new Exception("Не удалось получить информацию о процессоре");
		}


		public static bool RegDataIsTrue(RegData regData)
		{
			//string email = regData.Email;

			//string md5HashEmil = EasyEncryption.MD5.ComputeMD5Hash(email);
			//string md5HashMashineName = EasyEncryption.MD5.ComputeMD5Hash(regData.Machine);

			//   string key = "";
			//   for (int i = 0; i < md5HashEmil.Length; i++)
			//   {
			//    key += md5HashEmil[i] ^ 3 & md5HashMashineName[i] ^ 7;
			//   }
			List<string> keys = new List<string>();
			for (int i = 0; i < 1000; i++)
			{
				keys.Add(string.Join("", EasyEncryption.MD5.ComputeMD5Hash("workZillaKeyGenerator" + i + "workzillaBot").Select(el => el ^ 3).Distinct()));
			}

			return keys.Any(el => el == regData.Key) || regData.Key == RegData._developerKey;
		}
	}
}
